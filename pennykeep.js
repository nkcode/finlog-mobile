

function api_get(call,params,callback) {
  $('#ajax_overlay').show() 

  params.token = localStorage['api_token'];
  console.log("api_get: "+callback)
  $.get("http://"+localStorage['host']+"/api/"+call, params, function(resp) {
    $('#ajax_overlay').hide()
    console.log("response: "+resp.length)
    callback(resp)
  }).fail(function(err) {
    $('#ajax_overlay').hide()
    console.log(err)
    var err = JSON.parse(err.responseText)
    alert(err.error)
  })
}

function local_storage_or_param(name,val) {
  if(!val) {
    return localStorage[name]
  } else {
    localStorage[name] = val
    return val
  }
}

function show_login() {
  show_screen("login")
}

function show_screen(name) {
  if(name=='login') {
    $("[data-role=header] .ui-btn").hide()
  } else {
    $("[data-role=header] .ui-btn").show()
  }
  name = local_storage_or_param("active_screen",name)
  $(".screen").hide()
  $("#"+name+".screen").show()
  window.location.hash = name
}

function show_accounts() {
  window.location = "backbone.html#accounts"
  return;
  show_screen("accounts")
  api_get("folders_html",{},function(folders) {
    $("#accounts #list").html(folders)
    $("#accounts #list .transaction_create").addClass("ui-btn ui-btn-inline")

    var start_position;
    var accepted;
    $(".account").draggable( {
      distance: 0,
      revertDuration: 100,
      revert: true,
      
      start: function( event, ui ) {
        accepted = false;
        start_position = ui.position;
        console.log('start')
      },

      stop: function(event,ui) {
        console.log('stop')
        console.log(accepted)
        if(accepted) return;
        var from = $(event.target).attr("data-account-id")

        console.log(start_position)
        console.log(ui.position)
        if(ui.position.top == start_position.top && ui.position.left == start_position.left) {
          show_account_transactions(from)
          return;
        }
       }
    })
    
    $(".account").droppable({
      accept: ".account",
      hoverClass: "accept-hover",
      
      drop: function( event, ui ) {  
        accepted = true;
        console.log("drop")
        var from = $(ui.draggable).attr("data-account-id")
        var to = $(this).attr("data-account-id")
        console.log(this)
        console.log(to)
        show_transaction_create(from,to)
      }
    });

    $("#accounts #list .folder .header").addClass("unselectable")
    if(localStorage['shrinked_folders']) {
      shrinked_folders = JSON.parse(localStorage['shrinked_folders'])
      shrinked_folders.forEach(function(folder) {
         $(".folder[data-folder-type='"+folder+"'] .accounts").hide()
         $(".folder[data-folder-type='"+folder+"'] .header").addClass("shrinked")
      })
    }
  })
}

function show_account_transactions(account_id) {
  account_id = local_storage_or_param("account_transactions_account_id",account_id)
  show_screen("account_transactions")
  $("#account_transactions #list").html("")
  api_get("account_transactions_html",{account_id:account_id},function(html) {
    $("#account_transactions #list").html(html)
  })

}

function show_transaction_create(from_account_id,to_account_id) {
  from_account_id = local_storage_or_param("transaction_create_from_account_id",from_account_id)
  to_account_id = local_storage_or_param("transaction_create_to_account_id",to_account_id)
  show_screen("transaction_create")
  api_get("transaction_create_html",{from_account_id:from_account_id,to_account_id:to_account_id},function(html) {
    $("#transaction_create.screen #form").html(html)
  })
}

function show_transaction_edit(transaction_id) {
  if(!transaction_id) {
    transaction_id=localStorage['edit_transaction_id']
  } else {
    localStorage['edit_transaction_id'] = transaction_id
  }

  show_screen("transaction_edit")
  console.log("editing transaction id: "+transaction_id)
  api_get("transaction_edit_html",{transaction_id:transaction_id},function(html) {
    $("#transaction_edit.screen #form").html(html)
  })
}

$(function() {
      document.addEventListener("deviceready", onDeviceReady, false);

      function onDeviceReady() {
          document.addEventListener("backbutton", onBackButton, false);
          return false;
      }

      function onBackButton() {
          alert("backbutton");
          return false;
      }




    if(localStorage['api_token']) {
      show_accounts()
/*      if(localStorage['active_screen']) {
        eval("show_"+localStorage['active_screen']+"()")
      }*/
    }

    $("#email").val(localStorage['email'])
    $("#pass").val(localStorage['pass'])
    $("#host").val(localStorage['host'])

   $("#authorize").click(function() {
     if(!localStorage['host']) {
      localStorage['host'] = 'pennykeep.cloudspaint.com'
     }
     
     var host = localStorage['host']
     var email = $("#email").val();
     var pass = $("#pass").val();
     if(!email) {
        alert("enter your email (it is used as username)");
        return false;
     }
     if(!pass) {
        alert("please enter password")
        return false;
     }

     $.get("http://"+host+"/api/authorize", {email: email, pass:pass}, function(resp) {
       resp = JSON.parse(resp)
       if(resp.user_id) {
          localStorage['api_token'] = resp.security_token;
          localStorage['email'] = email;
          localStorage['pass'] = pass;
          show_accounts()

       } else {
          alert(resp.error)
       }
     })

     return false;
   })

   $("#signout").click( function() {
      localStorage['api_token'] = null
      localStorage['pass'] = null
      show_screen("login")
   })

    $(document).on(click_event, ".folder .header", function() {
        var folder = $(this).parents(".folder")
        var folder_type = folder.attr("data-folder-type");
        console.log(folder_type)
        folder.find(".accounts").toggle()
        
        if(!localStorage['shrinked_folders']) {
          localStorage['shrinked_folders'] = JSON.stringify([])
        }

        shrinked_folders = JSON.parse(localStorage['shrinked_folders'])

        if(!folder.find(".accounts").is(":visible")) {
          $(this).addClass("shrinked")
          shrinked_folders.push(folder_type)
        } else {
          $(this).removeClass("shrinked")
          i = shrinked_folders.indexOf(folder_type)
          if(i>-1) {
            shrinked_folders.splice(i,1)
          }
        }
        console.log("shrinked folders")
        console.log(shrinked_folders)
        localStorage['shrinked_folders'] = JSON.stringify(shrinked_folders)

    });

    $(document).on(click_event,"#transaction_create #create", function() {
      var transaction_attributes = {}
      var attributes = ['from_account_id','to_account_id','amount','currency','note']
      attributes.forEach(function(attr) { 
        transaction_attributes["transaction["+attr+"]"] = $("#transaction_create [name='transaction["+attr+"]']").val()
      })
      console.log(transaction_attributes)
      api_get("transaction_create", transaction_attributes, function(resp) {
        JSON.parse(resp);
        if(resp.error) {
          alert(resp.error)
          return;
        }
        show_accounts()
      })
    })

    $(document).on(click_event,".transaction div",function() {
      var transaction_id = $(this).parents(".transaction").attr("data-transaction-id")
      show_transaction_edit(transaction_id)
    })

    $(document).on(click_event,"#transaction_edit #delete", function() {
      if(!confirm("Delete transaction?")) {
        return;
      }
      api_get("transaction_delete",{transaction_id:localStorage['edit_transaction_id']},function(resp) {
        window.history.back()
        console.log(resp)
      })
    })

    $(document).on(click_event,"#transaction_edit #update", function() {
      var transaction_attributes = {}
      var attributes = ['from_account_id','to_account_id','amount','currency','note','created_at']
      attributes.forEach(function(attr) { 
        transaction_attributes["transaction["+attr+"]"] = $("#transaction_edit [name='transaction["+attr+"]']").val()
      })
      transaction_attributes['transaction_id'] = localStorage['edit_transaction_id']
      console.log(transaction_attributes)
      api_get("transaction_update",transaction_attributes,function(resp) {
        window.history.back()
        console.log(resp)
      })
    })

    $(document).on("keydown","#transaction_edit input, #transaction_edit textarea, #transaction_edit select, #transaction_edit option",function(event) {
      if(event.keyCode==13) {
        $("#transaction_edit #update").trigger(click_event)
      }
      if(event.keyCode==27) {
        window.history.back()
      }
    })

    $(document).on("keydown","#transaction_create input, #transaction_create textarea, #transaction_create select, #transaction_create option",function(event) {
      if(event.keyCode==13) {
        $("#transaction_create #create").trigger(click_event)
      }
      if(event.keyCode==27) {
        window.history.back()
      }
    })


    $(window).bind( 'hashchange', function(e) {
       var hash = window.location.hash.replace(/^#/,"")
       if(localStorage['active_screen']!=hash) {
         cmd = "show_"+hash+"()"
         console.log("eval: "+cmd)
         eval(cmd)
       }
    });

})

 
 
