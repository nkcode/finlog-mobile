'use strict';


var Stat = Backbone.Model.extend()
var Stats = Backbone.Collection.extend( {
	model: Stat
})

var Account = Backbone.Model.extend()
var Accounts = Backbone.Collection.extend( {
	model: Account
})

var Currency = Backbone.Model.extend()
var Currencies = Backbone.Collection.extend( {
  model: Currency
})

var Transaction = Backbone.Model.extend()
