'use strict';

function api_get(call,params,callback) {
	console.log("api_get "+call)
  $('#ajax_overlay').show() 

  params.token = localStorage['api_token'];
  $.ajax({ 
      url: "http://"+localStorage['host']+"/api/"+call, 
      data: params, 
      success: function(resp) {
        $('#ajax_overlay').hide()
        console.log("response length: "+resp.length)
        resp = JSON.parse(resp)
        console.log(resp)
        callback(resp)
      },
      error: function(resp) {
        $('#ajax_overlay').hide()
        alert('error while accessing server')
        console.log(err)
        var err = JSON.parse(resp)
        alert(resp)
      }
  })
}


$(function() {
  Backbone.history.start()
})

$(function() {
    $(document).on(click_event, ".folder .toggle", function() {
        var folder = $(this).parents(".folder")
        var folder_type = folder.attr("data-folder-type");
        console.log(folder_type)
        folder.find(".accounts").toggle()
        
        if(!localStorage['shrinked_folders']) {
          localStorage['shrinked_folders'] = JSON.stringify([])
        }

        var shrinked_folders = JSON.parse(localStorage['shrinked_folders'])

        if(!folder.find(".accounts").is(":visible")) {
          $(this).addClass("shrinked")
          shrinked_folders.push(folder_type)
        } else {
          $(this).removeClass("shrinked")
          var i = shrinked_folders.indexOf(folder_type)
          if(i>-1) {
            shrinked_folders.splice(i,1)
          }
        }
        console.log("shrinked folders")
        console.log(shrinked_folders)
        localStorage['shrinked_folders'] = JSON.stringify(shrinked_folders)

    });
})

function apply_shrinked_folders() {
    if(localStorage['shrinked_folders']) {
      var shrinked_folders = JSON.parse(localStorage['shrinked_folders'])
      shrinked_folders.forEach(function(folder) {
         $(".folder[data-folder-type='"+folder+"'] .accounts").hide()
         $(".folder[data-folder-type='"+folder+"'] .header .toggle").addClass("shrinked")
      })
    }
}

function fetchAccounts(then) {
  if(window.accounts_cache) {
    return then();
  }
  api_get("accounts",{},function(resp) {
    window.accounts_cache = new Accounts(resp)
    then()
  })
}

function fetchCurrencies(then) {
  if(window.currencies) {
    return then();
  }
  api_get("currencies",{},function(resp) {
    window.currencies = new Currencies(resp)
    then()
  })

}


function accounts_draggable_setup() {
    var start_position;
    var accepted;
    $(".account").draggable( {
      distance: 0,
      revertDuration: 100,
      revert: true,
      
      start: function( event, ui ) {
        accepted = false;
        start_position = ui.position;
        console.log('start')
      },

      stop: function(event,ui) {
        console.log('stop')
        console.log(accepted)
        if(accepted) return;
        var from = $(event.target).attr("data-account-id")

        console.log(start_position)
        console.log(ui.position)
        if(ui.position.top == start_position.top && ui.position.left == start_position.left) {
          window.router.navigate("account_transactions/"+from,{trigger:true})
          return;
        }
       }
    })
    
    $(".account").droppable({
      accept: ".account",
      hoverClass: "accept-hover",
      
      drop: function( event, ui ) {  
        accepted = true;
        console.log("drop")
        var from = $(ui.draggable).attr("data-account-id")
        var to = $(this).attr("data-account-id")
        console.log(this)
        console.log(to)
        window.router.navigate("#transaction_create/"+from+"/"+to,{trigger:true})
      }
    });

}

function sign_out() {
    localStorage.removeItem('api_token')
    localStorage.removeItem('pass')
    window.location = "index.html#login"
}

//return sum extracted from comments like '1000 something 200 another'
function sum_from_note(comment) {
  var sums = _.collect(transactions,function(t) { return t.note.match(new RegExp("([0-9]+) "+comment,"i")) })  
  var sum = 0;
  _.each(sums,function(r) { 
    if(r) {
      sum += parseInt(r[1])
    }
  })
  return sum;
}


function sum_by_note(comment) {
  var sum = 0;
  _.each(transactions,function(t) { 
      if(t.note.match(new RegExp(comment,"i"))) {
        sum += t.amount;
      } 
  })
  return sum;
}