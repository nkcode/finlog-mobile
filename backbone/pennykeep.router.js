function show_navigation(which) {
  $(".navigation_bottom").hide()
  $(".navigation_bottom"+which).show()
}
  window.router = Backbone.Router.extend({
    routes: {
        "stats" : "stats",
        "stats/month/:type/:month_ago": "stats_month",
        "accounts" : "accounts",
        "transaction_create/:from_acc_id/:to_acc_id" : "transaction_create",
        "account_transactions/:acc_id(/:month_ago)": "account_transactions",
        "transaction_view/:id": "transaction_view"
    },
    stats: function() {
      show_navigation(".main")
    	statsView.render()
    },
    transaction_create: function(from_acc_id,to_acc_id) {
      show_navigation(".transaction_create");
      var transaction = new Transaction()
      transaction.attributes.from_account_id = from_acc_id;
      transaction.attributes.to_account_id = to_acc_id;
      transactionView.render(transaction)
    },
    transaction_view: function(transaction_id) {
      show_navigation(".transaction_edit");
      api_get("transaction",{id:transaction_id},function(resp) {
        var transaction =  new Transaction(resp)
        transactionView.render(transaction)
      })
    },
    stats_month: function(type,month_ago) {
      api_get("month_stats",{type:type,month_ago:month_ago },function(resp) {
        $("#content").html($("#month_stats_template").html())
        var max = 0;
        _.each(resp,function(rec) {
          max = max > rec.amount ? max : rec.amount;
        })
        console.log("maximum: "+max)
        var line_template = _.template($("#month_stats_line").html())
        _.each(resp,function(line) {
          $("#content table").append(line_template({rec:line,max:max,month_ago:month_ago}))
        })
      })
    },
    stats_month_income: function(month_ago) {

    },
    account_transactions: function(acc_id,month_ago) {
      show_navigation(".main")
      accountTransactionsView.render(acc_id,month_ago)
    },
    accounts: function() {
      show_navigation(".main")
      accountsView.render()
    }
  })

window.router = new window.router()
