'use strict';
$(function() {


	window.statsView = Backbone.View.extend( {
	    el: $("#content"),
	    line_template: _.template($("#stats_line_template").html()),
	    render: function() {
	    	var self = this;
	    	self.$el.html("")
	    	api_get("last_months_stats",{},function(resp) {
	    		var stats = new Stats(resp.lines)
          var line_index = 0;
	    		stats.each(function(r) {
	          self.$el.append(self.line_template({rec:r.attributes,max:resp.max_value, line_index:line_index}))
            line_index ++
	    		})

	    	})
	    	return this;

	    }
	  })

  window.statsView = new window.statsView()

  window.accountsView = Backbone.View.extend( {
  	el: "#content",
  	folder_template: _.template($("#acc_folder_template").html()),
  	account_template: _.template($("#account_template").html()),
  	render: function() {
  		var self = this;
  		self.$el.html("")
  		api_get("folders",{},function(folders) {
        window.folders = folders;
        window.accounts = {};
        console.log("accounts by folders saved to window.folders")
        console.log("accounts by name saved to accounts hash")
  			console.log(folders)  			
  			_.each(folders,function(r) {
  				console.log(r)
  				self.$el.append(self.folder_template({rec:r}))
  				var folder = _.last(self.$(".folder .accounts"))
  				_.each(r.accounts,function(acc) {
            window.accounts[acc.acc.name] = acc.acc;
            var rec = acc.acc;
            rec.current_amount = Math.round(acc.money_value);
            $(folder).append(self.account_template({rec:rec}))
  				})
  			})
        apply_shrinked_folders()
        accounts_draggable_setup()
  		})
  	}
  })

  window.accountsView = new window.accountsView()

  window.transactionView = Backbone.View.extend( {
    el: "#content",
    template: _.template($("#transaction_template").html()),
    events: {
      'submit form' : 'submit'
    },
    submit: function(e) {
      e.preventDefault()
      if(this.model.id) {
        this.updateTransaction()
      } else {
        this.createTransaction()
      }
    },
    updateTransaction: function() {
      this.updateModel()

      api_get("transaction_update",{
        transaction_id:this.model.id,
        transaction:this.model.attributes},function(resp) {
        window.history.back()
      })
    },
    updateModel: function() {
      this.model.attributes.from_account_id = this.$("#from_account_id").val()
      this.model.attributes.to_account_id = this.$("#to_account_id").val()
      this.model.attributes.amount = this.$("#amount").val()
      this.model.attributes.currency = this.$("input[name=currency]:checked").val()
      this.model.attributes.note = this.$("#note").val()
      if(this.model.id) {
        this.model.attributes.created_at = this.$("#created_at").val()
      }
      console.log(this.model.attributes)
    },

    deleteTransaction: function() {
      if(!confirm("Delete transaction. Are you sure?")) {
        return;
      }
      api_get("transaction_delete",{transaction_id:transactionView.model.id},function(resp) {
        window.router.navigate("accounts",{trigger:true})

      })
    },

    createTransaction: function() {

      this.updateModel()

      api_get("transaction_create",{ transaction: this.model.attributes   }, function(resp) {
          if(resp.error) {
            alert(resp.error)
          } else {
            window.router.navigate("#accounts",{trigger: true})
          }
        }
      )

    },
    render: function(transaction) {

      this.model = transaction;

      var self = this;
      self.$el.html(self.template({rec:transaction}))
      setTimeout(function() { self.$("#amount").focus() }, 200)

      fetchAccounts(function() {

        fetchCurrencies(function() {
          var selected;
          if(transaction.id) {
            selected = transaction.attributes.currency || 'usd'
          } else {
            var account = window.accounts_cache.findWhere({id:parseInt(transaction.attributes.from_account_id)});
            selected = account.attributes.currency;
          }
          self.$("#currency_input").html(
              _.template($("#transaction_currency_select").html()) ({recs:window.currencies.models, selected: selected})
            )
        })


        self.$("#from_account_id").html(
            _.template($("#transaction_account_select").html()) ({recs:window.accounts_cache.models,selected: transaction.attributes.from_account_id})
          )

        self.$("#to_account_id").html(
            _.template($("#transaction_account_select").html()) ({recs:window.accounts_cache.models,selected: transaction.attributes.to_account_id})
          )


      })



      return this;
    }
  })

  window.transactionView = new window.transactionView()


  window.accountTransactionsView = Backbone.View.extend( {
    el: "#content",
    template: _.template($("#account_transactions").html()),
    line_template: _.template($("#account_transaction_template").html()),
    events: {
      "click td.edit_trigger":        "transactionView",
      "touchstart td.edit_trigger":        "transactionView"
    },
    transactionView: function(ev) {
      window.router.navigate("transaction_view/"+$(ev.target).parent('tr').attr("data-rec-id"),{trigger:true})
    },
    render: function(acc_id,month_ago) {
      var self = this;
      api_get("account_transactions",{account_id:acc_id,month_ago:month_ago},function(recs) { 
        console.log("got transactions saved to window.transactions")
        console.log("use functions sum_by_note and sum_from_note")
        window.transactions = recs;
        self.$el.html(self.template())
        _.each(recs,function(rec) {
          $("table").append(self.line_template({rec:rec,acc_id:acc_id}))
        })
      })

    }

  })

  window.accountTransactionsView = new window.accountTransactionsView()


})
  